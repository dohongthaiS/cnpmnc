﻿using Report.report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebSockets;
using System.Windows.Forms;

namespace Report
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public void Load()
        {
            demo rpt = new demo();
            Form1 f1 = new Form1();
            f1.crystalReportViewer1.ReportSource = rpt;
            f1.ShowDialog();
        }
        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Load();
        }
        public void Load1()
        {
            demo1 rpt = new demo1();
            Form1 f1 = new Form1();
            f1.crystalReportViewer1.ReportSource = rpt;
            f1.ShowDialog();
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            Load1();
        }
        public void Load2()
        {
            demo2 rpt = new demo2();
            Form1 f1 = new Form1();
            f1.crystalReportViewer1.ReportSource = rpt;
            f1.ShowDialog();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Load2();
        }
        public void Load3()
        {
            demo3 rpt = new demo3();
            Form1 f1 = new Form1();
            f1.crystalReportViewer1.ReportSource = rpt;
            f1.ShowDialog();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            Load3();
        }
        public void Load4()
        {
            demo4 rpt = new demo4();
            Form1 f1 = new Form1();
            f1.crystalReportViewer1.ReportSource = rpt;
            f1.ShowDialog();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            Load4();
        }
        public void Load5()
        {
            demo5 rpt = new demo5();
            Form1 f1 = new Form1();
            f1.crystalReportViewer1.ReportSource = rpt;
            f1.ShowDialog();
        }
        private void button5_Click(object sender, EventArgs e)
        {
            Load5();
        }
        public void Load6()
        {
            demo6 rpt = new demo6();
            Form1 f1 = new Form1();
            f1.crystalReportViewer1.ReportSource = rpt;
            f1.ShowDialog();
        }
        private void button6_Click(object sender, EventArgs e)
        {
            Load6();
        }
        public void Load7()
        {
            demo7 rpt = new demo7();
            Form1 f1 = new Form1();
            f1.crystalReportViewer1.ReportSource = rpt;
            f1.ShowDialog();
        }
        private void button7_Click(object sender, EventArgs e)
        {
            Load7();
        }
        public void Load8()
        {
            demo8 rpt = new demo8();
            Form1 f1 = new Form1();
            f1.crystalReportViewer1.ReportSource = rpt;
            f1.ShowDialog();
        }
        private void button8_Click(object sender, EventArgs e)
        {
            Load8();
        }
        public void Load9()
        {
            demo9 rpt = new demo9();
            Form1 f1 = new Form1();
            f1.crystalReportViewer1.ReportSource = rpt;
            f1.ShowDialog();
        }
        private void button9_Click(object sender, EventArgs e)
        {
            Load9();
        }
    }
}
